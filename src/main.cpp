#include <cmath>
#include <format>
#include <iostream>
#include <limits>
#include <numbers>
#include <random>
#include <vector>

#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>
#include <SFML/Graphics.hpp>

// inline float int_pow(float base, int pwr)
// {
//     float ans { 1.f };
//     for (int x { 0 }; x < pwr; ++x) {
//         ans *= base;
//     }

//     return ans;
// }

// inline float pythag(sf::Vector2f vec)
// {
//     return vec.x * vec.x + vec.y * vec.y;
// }

inline float dot(const sf::Vector2f& v1, const sf::Vector2f& v2)
{
    return v1.x * v2.x + v1.y * v2.y;
}

inline float cross(const sf::Vector2f& v1, const sf::Vector2f& v2)
{
    return v1.x * v2.y - v1.y * v2.x;
}

inline float norm(const sf::Vector2f& vec)
{
    return std::sqrt(vec.x * vec.x + vec.y * vec.y);
}

inline void scale(sf::Vector2f& vec, float new_length)
{
    float mag { norm(vec) };
    if (mag != 0) {
        vec *= new_length / mag;
    }
}

class Surround : public sf::RectangleShape {
private:
    float left, right, top, bottom;

public:
    Surround(const sf::Vector2f& pos, const sf::Vector2f& size)
        : sf::RectangleShape(size)
        , left(pos.x - 0.5f * size.x)
        , right(pos.x + 0.5f * size.x)
        , top(pos.y - 0.5f * size.y)
        , bottom(pos.y + 0.5f * size.y)
    {
        this->setOrigin(0.5f * size);
        this->setPosition(pos);
        this->setFillColor(sf::Color::Transparent);
        this->setOutlineThickness(10.f);
        this->setOutlineColor(sf::Color::White);
    }

    float getLeft() const
    {
        return this->left;
    }

    float getRight() const
    {
        return this->right;
    }

    float getTop() const
    {
        return this->top;
    }

    float getBottom() const
    {
        return this->bottom;
    }
};

class Segment : public sf::VertexArray, public sf::Transformable {
public:
    Segment(const sf::Vector2f& p1, const sf::Vector2f& p2)
        : sf::VertexArray(sf::Lines, 2)
    {
        (*this)[0] = sf::Vertex(p1);
        (*this)[1] = sf::Vertex(p2);
    }

    Segment(const sf::Vector2f& p1, const sf::Vector2f& p2, sf::Color col)
        : sf::VertexArray(sf::Lines, 2)
    {
        (*this)[0] = sf::Vertex(p1, col);
        (*this)[1] = sf::Vertex(p2, col);
    }

    sf::FloatRect getGlobalBounds() const
    {
        return this->getTransform().transformRect(this->getBounds());
    }
};

struct Behaviour {
    float alignment;
    float cohesion;
    float separation;

    float target;

    float internal_avoidance;
    float external_avoidance;

    float min_speed;
    float max_speed;
    float max_acc;
    float general_perception;
    float forward_perception;

    sf::Vector2f targetPos;

    Behaviour()
        : alignment(2e2f)
        , cohesion(1e2f)
        , separation(2e2f)
        , target(1e2f)
        , internal_avoidance(1e2f)
        , external_avoidance(1e2f)
        , min_speed(1e2f)
        , max_speed(5e2f)
        , max_acc(3e2f)
        , general_perception(3e2f)
        , forward_perception(1e3f)
        , targetPos()
    {
    }
};

class Boid : public sf::ConvexShape {
private:
    sf::Vector2f velocity;
    sf::Vector2f acceleration;

public:
    Boid(sf::Vector2f pos, sf::Vector2f vel, sf::Color colour)
        : velocity(vel)
        , acceleration()
    {
        const float scale { 10.f };
        this->setPointCount(3);
        this->setPoint(0, sf::Vector2f(0.f, 0.f));
        this->setPoint(1, sf::Vector2f(std::numbers::phi_v<float>, 0.5f));
        this->setPoint(2, sf::Vector2f(0.f, 1.f));
        this->setScale(sf::Vector2f(scale, scale));
        this->setOrigin(0.5f * std::numbers::phi_v<float>, 0.5f);
        this->setPosition(pos);
        this->setFillColor(colour);
    }

    void tick(float dt, const Behaviour& settings, const sf::RenderWindow& main_window)
    {
        const float mag_a { norm(this->getAcc()) };
        if (mag_a > settings.max_acc) {
            this->setAcc(this->getAcc() / mag_a * settings.max_acc);
        }

        this->setVel(this->getVel() + this->acceleration * dt);

        const float mag_v { norm(this->getVel()) };
        if (mag_v > settings.max_speed) {
            this->setVel(this->getVel() / mag_v * settings.max_speed);
        } else if (mag_v > settings.min_speed) {
            this->velocity *= 0.999f;
        }

        this->move(this->getVel() * dt);

        // const sf::FloatRect boundingBox { this->getGlobalBounds() };
        // const sf::Vector2f view { main_window.getView().getSize() };

        // // periodic boundaries
        // const float a { 50.f };
        // if (boundingBox.left > view.x) {
        //     this->setPosition(a - boundingBox.width, this->getPosition().y);
        // } else if (boundingBox.left + boundingBox.width < 0.f) {
        //     this->setPosition(view.x + boundingBox.width - a, this->getPosition().y);
        // }
        // if (boundingBox.top > view.y) {
        //     this->setPosition(this->getPosition().x, a - boundingBox.height);
        // } else if (boundingBox.top + boundingBox.height < 0.f) {
        //     this->setPosition(this->getPosition().x, view.y + boundingBox.height - a);
        // }
        // // end

        // if (boundingBox.left > view.x || boundingBox.left + boundingBox.width < 0.f) {
        //     this->velocity.x *= -1.f;
        // }
        // if (boundingBox.top > view.y || boundingBox.top + boundingBox.height < 0.f) {
        //     this->velocity.y *= -1.f;
        // }

        this->acceleration *= 0.9f;
    }

    void setVel(const sf::Vector2f& v)
    {
        this->setRotation(180.f * (std::atan2(v.y, v.x) / std::numbers::pi_v<float>));
        this->velocity = v;
    }

    void setAcc(const sf::Vector2f& a)
    {
        this->acceleration = a;
    }

    void adjAcc(const sf::Vector2f& a)
    {
        this->acceleration += a;
    }

    sf::Vector2f getVel() const
    {
        return velocity;
    }

    sf::Vector2f getAcc() const
    {
        return acceleration;
    }
};

struct Camera {
    static constexpr float init_vel { 10.f };
    static constexpr float zoom_vel { 1.02f };
    static constexpr float rotate_vel { 1.f };
    float translate_vel { init_vel };

    void reset_vel()
    {
        this->translate_vel = this->init_vel;
    }
};

struct Range {
    sfg::Box::Ptr container;
    sfg::Label::Ptr label;
    sfg::Label::Ptr value;
    sfg::Scale::Ptr hscale;
    sfg::Box::Ptr scalebox;
    sfg::Adjustment::Ptr adjustment;

    Range(std::string label_text, float& quantity)
        : container(sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL))
        , label(sfg::Label::Create())
        , value(sfg::Label::Create())
        , hscale(sfg::Scale::Create(sfg::Scale::Orientation::HORIZONTAL))
        , scalebox(sfg::Box::Create(sfg::Box::Orientation::VERTICAL))
        , adjustment(hscale->GetAdjustment())
    {
        label->SetText(label_text);

        SetValueText(quantity);

        adjustment->SetLower(0.f);
        adjustment->SetUpper(5e2f);

        adjustment->GetSignal(sfg::Adjustment::OnChange).Connect([this, &quantity] {
            quantity = this->adjustment->GetValue();
            this->SetValueText(quantity);
        });

        hscale->SetRequisition(sf::Vector2f(80.f, 20.f));

        scalebox->Pack(hscale, false, false);

        container->Pack(label);
        container->Pack(scalebox);
        container->Pack(value);

        container->SetSpacing(5.f);
    }

    void SetValueText(float number)
    {
        value->SetText(std::format("{:.0f}", number));
    }
};

struct Label {
    sfg::Label::Ptr label;

    Label(std::string text)
        : label(sfg::Label::Create())
    {
        label->SetText(text);
    }
};

int main()
{
    std::cout << "START\n";

    sf::Font font;
    font.loadFromFile("../res/font/Hack-Regular.ttf");

    sf::ContextSettings window_settings;
    // window_settings.antialiasingLevel = 2;

    const int window_width { 3000 };
    const int window_height { 2000 };

    sf::RenderWindow main_window(sf::VideoMode(window_width, window_height), "Boids", sf::Style::Default, window_settings);
    main_window.setVerticalSyncEnabled(true);

    Behaviour weights;

    // { GUI

    sfg::SFGUI sfgui;
    // main_window.resetGLStates();

    auto window = sfg::Window::Create();
    window->SetTitle("Weights");

    auto container = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);

    Range box_sep("Separation", weights.separation);
    Range box_coh("Cohesion", weights.cohesion);
    Range box_ali("Alignment", weights.alignment);
    Range box_tar("Target", weights.target);
    Range box_per("Perception", weights.general_perception);
    Range box_vmax("Maximum Speed", weights.max_speed);
    Range box_amax("Maximum Acceleration", weights.max_acc);

    Label targ("Left click on screen with mouse to set target position.");
    Label nav("Move view with WASD.");
    Label rot("Rotate (anti-)clockwise with (Q) E.");
    Label zoom("Zoom in (out) with Z (X).");
    Label reset("Reset view with C.");

    container->Pack(box_sep.container);
    container->Pack(box_coh.container);
    container->Pack(box_ali.container);
    container->Pack(box_tar.container);
    container->Pack(box_per.container);
    container->Pack(box_vmax.container);
    container->Pack(box_amax.container);
    container->Pack(targ.label);
    container->Pack(nav.label);
    container->Pack(rot.label);
    container->Pack(zoom.label);
    container->Pack(reset.label);

    container->SetSpacing(5.f);

    window->Add(container);

    // } end

    // sf::RenderWindow config_window(sf::VideoMode(1000, 1000), "Configuration");
    // config_window.setVerticalSyncEnabled(true);

    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(24);
    text.setFillColor(sf::Color::White);

    const float area_size { 1000.f };
    const float margin_left { 0.5f * (window_width - area_size) };
    const float margin_right { window_width - margin_left };
    const float margin_top { 0.5f * (window_height - area_size) };
    const float margin_bottom { window_height - margin_left };
    const float max_init_speed { 500.f };

    std::random_device dev;
    std::mt19937_64 rng(dev());
    std::uniform_real_distribution<float> dist_size(20.f, 200.f);
    std::uniform_real_distribution<float> dist_x(margin_left, margin_right);
    std::uniform_real_distribution<float> dist_y(margin_top, margin_bottom);
    std::uniform_real_distribution<float> dist_v(-max_init_speed, max_init_speed);
    std::uniform_int_distribution<unsigned short> dist_c(0, std::numeric_limits<unsigned short>::max());

    const int number { 1000 };

    std::vector<Boid> boids;
    for (int i { 0 }; i < number; ++i) {
        const sf::Vector2f pos(dist_x(rng), dist_y(rng));
        const sf::Vector2f vel(dist_v(rng), dist_v(rng));
        // const sf::Vector2f vel(sf::Vector2f(300.f, 150.f));
        const sf::Color colour(dist_c(rng), dist_c(rng), dist_c(rng));
        Boid boid(pos, vel, colour);
        boids.push_back(boid);
    }

    // sf::Vector2f middle { 0.5f * static_cast<sf::Vector2f>(main_window.getSize()) };
    // Surround boundary(middle, sf::Vector2f(area_size, area_size));

    const int no_obs { 0 };

    std::vector<sf::CircleShape> obstacles;
    for (int i { 0 }; i < no_obs; ++i) {
        float size { dist_size(rng) };
        sf::CircleShape obstacle(size);
        obstacle.setFillColor(sf::Color::Red);
        obstacle.setOrigin(0.5f * size, 0.5f * size);
        obstacle.setPosition(dist_x(rng), dist_y(rng));
        obstacles.push_back(obstacle);
    }

    sf::Clock clock;

    Camera camera;

    while (main_window.isOpen()) {
        float delta { clock.restart().asSeconds() };

        window->Update(delta);

        sf::Event event;

        while (main_window.pollEvent(event)) {
            window->HandleEvent(event);

            if (event.type == sf::Event::Closed)
                main_window.close();
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            weights.targetPos = static_cast<sf::Vector2f>(sf::Mouse::getPosition());
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z)) {
            sf::View newView { main_window.getView() };
            newView.zoom(1.f / camera.zoom_vel);
            main_window.setView(newView);
            camera.translate_vel /= camera.zoom_vel;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::X)) {
            sf::View newView { main_window.getView() };
            newView.zoom(camera.zoom_vel);
            main_window.setView(newView);
            camera.translate_vel *= camera.zoom_vel;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
            sf::View newView { main_window.getView() };
            newView.move(0.f, camera.translate_vel);
            main_window.setView(newView);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
            sf::View newView { main_window.getView() };
            newView.move(0.f, -camera.translate_vel);
            main_window.setView(newView);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
            sf::View newView { main_window.getView() };
            newView.move(-camera.translate_vel, 0.f);
            main_window.setView(newView);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            sf::View newView { main_window.getView() };
            newView.move(camera.translate_vel, 0.f);
            main_window.setView(newView);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
            sf::View newView { main_window.getView() };
            newView.rotate(-camera.rotate_vel);
            main_window.setView(newView);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::E)) {
            sf::View newView { main_window.getView() };
            newView.rotate(camera.rotate_vel);
            main_window.setView(newView);
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::C)) {
            sf::View newView(sf::FloatRect(0.f, 0.f, window_width, window_height));
            main_window.setView(newView);
            camera.reset_vel();
        }

        // if (sf::Keyboard::isKeyPressed(sf::Keyboard::R)) {
        //     p.decrease_update_period();
        // }

        // if (sf::Keyboard::isKeyPressed(sf::Keyboard::F)) {
        //     p.increase_update_period();
        // }

        // if (sf::Keyboard::isKeyPressed(sf::Keyboard::V)) {
        //     p.reset_update_period();
        // }

        main_window.clear(sf::Color::Black);

        sfgui.Display(main_window);

        // main_window.draw(boundary);

        for (const sf::CircleShape& obstacle : obstacles) {
            main_window.draw(obstacle);
        }

        text.setPosition(main_window.mapPixelToCoords(sf::Vector2i(main_window.getSize().x - 60, 0)));
        text.setString(std::format("{:.1f}", 1.f / delta));
        main_window.draw(text);

        for (Boid& boid : boids) {

            sf::Vector2f toTarget { weights.targetPos - boid.getPosition() };
            scale(toTarget, weights.target);
            boid.adjAcc(toTarget);

            // Segment origin_line(boid.getPosition(), boid.getPosition() + toTarget, boid.getFillColor());
            // main_window.draw(origin_line);

            // sf::Vector2f heading { boid.getVel() / norm(boid.getVel()) };                        // local
            // sf::Vector2f waypoint { boid.getPosition() + heading * weights.forward_perception }; // global

            // sf::CircleShape point(5.f);
            // point.setFillColor(boid.getFillColor());
            // point.setPosition(waypoint);
            // main_window.draw(point);

            // if (waypoint.x < boundary.getLeft() || waypoint.x > boundary.getRight() || waypoint.y < boundary.getTop() || waypoint.y > boundary.getBottom()) {
            //     sf::Vector2f toMiddle { middle - boid.getPosition() };  // local
            //     sf::Vector2f compass(boid.getVel().y, boid.getVel().x); // local
            //     // circular rules
            //     const float sinTheta { cross(boid.getVel(), toMiddle) };
            //     if (sinTheta >= 0.f) {
            //         // CCW
            //         compass.x *= -1;
            //     } else {
            //         // CW
            //         compass.y *= -1;
            //     }
            //     scale(compass, weights.internal_avoidance);

            //     // Segment centre_line(boid.getPosition(), boid.getPosition() + compass, boid.getFillColor());
            //     // main_window.draw(centre_line);

            //     boid.adjAcc(compass);
            // }

            // Segment toWaypoint(boid.getPosition(), waypoint);
            // // Segment toWaypoint(boid.getPosition(), waypoint, boid.getFillColor());
            // // main_window.draw(toWaypoint);

            // for (sf::CircleShape obstacle : obstacles) {
            //     if (toWaypoint.getGlobalBounds().intersects(obstacle.getGlobalBounds())) {
            //         sf::Vector2f obstacle_centre { obstacle.getPosition() - boid.getPosition() }; // local
            //         sf::Vector2f closest_line_point { norm(obstacle_centre) * heading };          // local
            //         sf::Vector2f steer_avoid { closest_line_point - obstacle_centre };            // local
            //         scale(steer_avoid, weights.external_avoidance);
            //         // Segment steer_line(boid.getPosition(), steer_avoid, boid.getFillColor());
            //         // main_window.draw(steer_line);
            //         // boid.adjAcc(steer_avoid);
            //     }
            // }

            sf::Vector2f escape_dir;                   // separation
            sf::Vector2f group_pos;                    // cohesion
            sf::Vector2f group_vel { sf::Vector2f() }; // alignment
            float visible { 0.f };
            for (const Boid& other : boids) {
                sf::Vector2f direction { boid.getPosition() - other.getPosition() };
                float separation { norm(direction) };
                if (separation < weights.general_perception) {
                    group_vel += other.getVel();
                    group_pos += other.getPosition();
                    if (separation != 0.f) {
                        direction /= separation;
                        escape_dir += direction;
                    }
                    visible += 1.f;
                }
            }

            // sf::CircleShape visibility(weights.general_perception);
            // visibility.setFillColor(sf::Color::Transparent);
            // visibility.setOutlineColor(boid.getFillColor());
            // visibility.setOutlineThickness(1.f);
            // visibility.setOrigin(weights.general_perception, weights.general_perception);
            // visibility.setPosition(boid.getPosition());
            // main_window.draw(visibility);

            if (visible > 1.f) {
                // escape_dir /= visible;
                scale(escape_dir, weights.max_speed);
                sf::Vector2f steer_separate { escape_dir - boid.getVel() };
                scale(steer_separate, weights.separation);
                // Segment separate_line(boid.getPosition(), boid.getPosition() + steer_separate, boid.getFillColor());
                // main_window.draw(separate_line);

                group_pos /= visible;
                sf::Vector2f centre { group_pos - boid.getPosition() };
                scale(centre, weights.max_speed);
                sf::Vector2f steer_cohese { centre - boid.getVel() };
                scale(steer_cohese, weights.cohesion);
                // Segment cohese_line(boid.getPosition(), boid.getPosition() + steer_cohese, boid.getFillColor());
                // main_window.draw(cohese_line);

                // group_vel /= visible;
                scale(group_vel, weights.max_speed);
                sf::Vector2f steer_align { group_vel - boid.getVel() };
                scale(steer_align, weights.alignment);

                // Segment align_line(boid.getPosition(), boid.getPosition() + group_vel, boid.getFillColor());
                // Segment align_line(boid.getPosition(), boid.getPosition() + steer_align, boid.getFillColor());
                // main_window.draw(align_line);

                boid.adjAcc(steer_separate);
                boid.adjAcc(steer_cohese);
                boid.adjAcc(steer_align);
            }

            boid.tick(delta, weights, main_window);

            // Segment acc(boid.getPosition(), boid.getPosition() + boid.getAcc(), boid.getFillColor());
            // main_window.draw(acc);

            // Segment vel_vec(boid.getPosition(), boid.getPosition() + boid.getVel(), boid.getFillColor());
            // main_window.draw(vel_vec);

            main_window.draw(boid);
        }

        main_window.display();
    }
}
