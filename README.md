# [boids](https://gitlab.com/eidoom/boids)
* [Companion post](https://computing-blog.netlify.app/post/boids/)
## Usage
### Compile
* Configure
    ```shell
    cd src
    meson setup ../build
    ```
* Build
    ```shell
    cd build
    meson compile
    ```
### Run
* Run 
    ```shell
    ./build/main
    ```
## Technology
* [Meson build system](https://mesonbuild.com/) ([Wikipedia](https://en.wikipedia.org/wiki/Meson_(software)))
* [C++](https://isocpp.org/) ([Wikipedia](https://en.wikipedia.org/wiki/C%2B%2B))
## Libraries
* [SFML](https://www.sfml-dev.org/) ([Wikipedia](https://en.wikipedia.org/wiki/Simple_and_Fast_Multimedia_Library))
* [SFGUI](https://github.com/TankOs/SFGUI)
    * Build
    ```shell
    cd GIT
    git clone git@github.com:TankOs/SFGUI.git
    cd BUILD
    cmake GIT/SFGUI
    make -j
    make install
    echo "export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH" >> ~/.*rc
    ```
## Reading
* [Flocks, Herds, and Schools: A Distributed Behavioral Model](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.103.7187&rep=rep1&type=pdf)
* [Craig Reynolds' webpage on Boids](http://www.red3d.com/cwr/boids/)
* [Wikipedia: Boids](https://en.wikipedia.org/wiki/Boids)
* [Medium: Simulating Bird Flock Behavior in Python Using Boids](https://medium.com/better-programming/boids-simulating-birds-flock-behavior-in-python-9fff99375118)
* [SFML GUI](https://blog.rubenwardy.com/2020/01/21/creating-game-guis-in-sfml/)
* [Video](https://youtu.be/bqtqltqcQhw)
## Resource attribution
* [Hack font](https://github.com/source-foundry/Hack)
